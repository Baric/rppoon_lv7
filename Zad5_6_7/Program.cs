﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5_6_7
{
    class Program
    {
        static void Main(string[] args)
        {
            DVD dvd = new DVD("Lord of The Rings", DVDType.MOVIE, 50);
            VHS vhs = new VHS("Godfather", 40);
            Book book = new Book("Silmarilion", 60);

            List<IVisitor> list = new List<IVisitor>();

            BuyVisitor visitor = new BuyVisitor();
            visitor.Visit(book);
            visitor.Visit(dvd);
            visitor.Visit(vhs);

            list.Add(visitor);
        }
    }
}
