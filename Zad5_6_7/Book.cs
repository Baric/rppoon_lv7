﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5_6_7
{
    class Book : IItem
    {
        public string Title 
        {
            get { return this.Title; }
            set {;}
        }
        public double Price 
        {
            get { return this.Price; }
            private set { ; } 
        }
        public Book(string title, double price)
        {
            this.Title = title;
            this.Price = price;
        }
        public override string ToString()
        {
           return "Book: " + this.Title +
           Environment.NewLine + "Price: " + this.Price;
        }
        public double Accept(IVisitor visitor) { return visitor.Visit(this); }
    }
}
