﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5_6_7
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
