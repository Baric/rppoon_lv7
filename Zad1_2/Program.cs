﻿using System;

namespace Zad1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence sequence = new NumberSequence(100);
            SortStrategy strategy = new BubbleSort();
            Random generator = new Random();

            for(int i = 0; i < 100; i++)
            {
                sequence.InsertAt(i, generator.Next(1, 10));
            }

            sequence.SetSortStrategy(strategy);
            sequence.Sort();
            Console.WriteLine(sequence.ToString());

            SearchNum search = new LinearSearch(10);
            sequence.SetSortStrategy(search);
            sequence.Search();

        }
    }
}
