﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Zad1_2
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            for(int i = 1; i < array.Length; i++)
            {
                bool Swap = false;

                for(int j = 0; j < array.Length - i; j++)
                {
                    if(array[j] > array[j + 1])
                    {
                        Swap = true;
                        double temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
                if (!Swap) { break; }
            }
        }
    }
}
