﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad1_2
{
    class LinearSearch : SearchNum
    {
        private double wantedNumber;
        public LinearSearch(double wantedNumber)
        {
            this.wantedNumber = wantedNumber;
        }

        public void Search(double[] array)
        {
            int num = array.Length;
            int isFound = 0;

            for(int i = 0; i < num; i++)
            {
                if(array[i] == wantedNumber)
                {
                    isFound = 1;
                }
            }        
        }
    }
}