﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3_4
{
    class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            string systemInformation = DateTime.Now + "CPU Load: " + provider.CPULoad + "Avaliable RAM: " + provider.AvailableRAM;
            Console.WriteLine(systemInformation);
        }
    }
}
