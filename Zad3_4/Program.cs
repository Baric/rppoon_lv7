﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider provider = new SystemDataProvider();

            FileLogger logger = new FileLogger("Test1.txt");
            FileLogger logger2 = new FileLogger("Test2.txt");

            ConsoleLogger log = new ConsoleLogger();

            provider.Attach(log);
            provider.Attach(logger);
            provider.Attach(logger2);

            while (true)
            {
                provider.GetAvailableRAM();
                provider.GetCPULoad();

                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}